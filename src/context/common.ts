// Utility  Functions

/**
 * Check the response object to make sure the response code is 200
 *
 * @param {Response} response  Response object ;
 */
export const checkResponse = ( response: Response ) => {
  if ( response.status !== 200 ) {
    throw response.statusText;
  }
  return response;
};
